package ncellappcamp.example.com.retrofit;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Date;



public class CustomAdapter extends BaseAdapter{

    Typeface weatherfont;
    String [] humidity;
    Context context;
    String [] rain;
    String [] icon;
    String [] time;
    private static LayoutInflater inflater=null;
    public CustomAdapter(ScrollingActivity mainActivity, String[] humidity, String[] rain,String[]icon,String[]time,Typeface weatherFont) {
        this.humidity=humidity;
        context=mainActivity;
        this.rain=rain;
        this.icon=icon;
        this.time=time;
        this.weatherfont =weatherFont;


    }
    @Override
    public int getCount() {
        return humidity.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder
    {
        TextView humidity;
        TextView rain;
        TextView icontext;
        TextView time;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder=new Holder();

        View rowView;

        rowView = inflater.inflate(R.layout.forecast, null);
        holder.humidity=(TextView) rowView.findViewById(R.id.humidity);
        holder.rain=(TextView) rowView.findViewById(R.id.rain_description);
        holder.time=(TextView) rowView.findViewById(R.id.time);
        holder.icontext=(TextView) rowView.findViewById(R.id.weather_icon_text);

        holder.icontext.setTypeface(weatherfont);

        switch (icon[position]){
            case "01d":
                holder.icontext.setText(R.string.wi_day_sunny);
                break;
            case "02d":
                holder.icontext.setText(R.string.wi_cloudy_gusts);
                break;
            case "03d":
                holder.icontext.setText(R.string.wi_cloud_down);
                break;
            case "10d":
                holder.icontext.setText(R.string.wi_day_rain_mix);
                break;
            case "11d":
                holder.icontext.setText(R.string.wi_day_thunderstorm);
                break;
            case "13d":
                holder.icontext.setText(R.string.wi_day_snow);
                break;
            case "01n":
                holder.icontext.setText(R.string.wi_night_clear);
                break;
            case "04d":
                holder.icontext.setText(R.string.wi_cloudy);
                break;
            case "04n":
                holder.icontext.setText(R.string.wi_night_cloudy);
                break;
            case "02n":
                holder.icontext.setText(R.string.wi_night_cloudy);
                break;
            case "03n":
                holder.icontext.setText(R.string.wi_night_cloudy_gusts);
                break;
            case "10n":
                holder.icontext.setText(R.string.wi_night_cloudy_gusts);
                break;
            case "11n":
                holder.icontext.setText(R.string.wi_night_rain);
                break;
            case "13n":
                holder.icontext.setText(R.string.wi_night_snow);
                break;
            default:
                holder.icontext.setText("-=");


        }


        long timestamp = Long.parseLong(time[position]);
        Date expiry = new Date(timestamp * 1000);
        holder.time.setText(String.valueOf(expiry));
        holder.humidity.setText(humidity[position]);
        holder.rain.setText(rain[position]);
        rowView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "You Clicked "+humidity[position], Toast.LENGTH_LONG).show();
            }
        });
        return rowView;
    }

}