package ncellappcamp.example.com.retrofit;

class Weather {
    String weathericon;
    String humidity;
    String raindescr;
    String time;

    Weather(String weathericon, String humidity, String raindescr, String time) {
        this.weathericon = weathericon;
        this.humidity = humidity;
        this.raindescr = raindescr;
        this.time = time;
    }
}