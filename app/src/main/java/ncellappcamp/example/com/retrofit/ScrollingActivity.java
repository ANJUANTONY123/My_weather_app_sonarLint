package ncellappcamp.example.com.retrofit;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


import ncellappcamp.example.com.retrofit.Model.WeatherData;
import ncellappcamp.example.com.retrofit.Service.APIManager;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;




public class ScrollingActivity extends AppCompatActivity {



    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private Typeface weatherFont;
    private String placelocation="";
    private ProgressBar spinner;
    TextView weatherReport;
    TextView  place;
    TextView  weathericon;
    TextView   country;

    List myList ;
    String APIKEY="5bfbbd7fbc02a39b9c0c2eb28331a00a";
    private  static String PATH= "fonts/weather.ttf";



    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_scrolling);
        spinner = (ProgressBar)findViewById(R.id.progressBar1);
        spinner.setVisibility(View.VISIBLE);

        weathericon=(TextView)findViewById(R.id.weather_icon);

        country=(TextView)findViewById(R.id.country);
        weatherFont = Typeface.createFromAsset(getAssets(), PATH);
        weathericon.setTypeface(weatherFont);





        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_id);


        mRecyclerView.setHasFixedSize(true);


        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);



        weatherReport= (TextView) findViewById(R.id.weather_report);
        place =(TextView)findViewById(R.id.place);

        GPSTracker gpsTracker = new GPSTracker(this);

        if (gpsTracker.getIsGPSTrackingEnabled())
        {
            String stringLatitude = String.valueOf(gpsTracker.latitude);
            String stringLongitude = String.valueOf(gpsTracker.longitude);

            APIManager.getApiService().getWeatherInfo(stringLatitude,
                    stringLongitude,
                    "10",
                    APIKEY,
                    callback);
        }
        else
        {


            gpsTracker.showSettingsAlert();
        }





        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);




        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "you are viewing weather of " + placelocation, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();


            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private Callback<WeatherData> callback = new Callback<WeatherData>() {
        public List<Weather> weathers;
        @Override
        public void success (WeatherData response, Response response2) {


            weatherReport.setText(response.getList().get(0).getWeather().get(0).getDescription());
            place.setText(response.getCity().getName());
            country.setText(response.getCity().getCountry());
            placelocation =response.getCity().getName();


            Log.w("icon", response.getList().get(0).getWeather().get(0).getIcon());
            switch (response.getList().get(0).getWeather().get(0).getIcon()){
                case "01d":
                    weathericon.setText(R.string.wi_day_sunny);
                    break;
                case "02d":
                    weathericon.setText(R.string.wi_cloudy_gusts);
                    break;
                case "03d":
                    weathericon.setText(R.string.wi_cloud_down);
                    break;
                case "04d":
                    weathericon.setText(R.string.wi_cloudy);
                    break;
                case "04n":
                    weathericon.setText(R.string.wi_night_cloudy);
                    break;
                case "10d":
                    weathericon.setText(R.string.wi_day_rain_mix);
                    break;
                case "11d":
                    weathericon.setText(R.string.wi_day_thunderstorm);
                    break;
                case "13d":
                    weathericon.setText(R.string.wi_day_snow);
                    break;
                case "01n":
                    weathericon.setText(R.string.wi_night_clear);
                    break;
                case "02n":
                    weathericon.setText(R.string.wi_night_cloudy);
                    break;
                case "03n":
                    weathericon.setText(R.string.wi_night_cloudy_gusts);
                    break;
                case "10n":
                    weathericon.setText(R.string.wi_night_cloudy_gusts);
                    break;
                case "11n":
                    weathericon.setText(R.string.wi_night_rain);
                    break;
                case "13n":
                    weathericon.setText(R.string.wi_night_snow);
                    break;
                default:
                    weathericon.setText(R.string.wi_night_cloudy);



            }
            String[]humidity = new String[10];
            String[]raindescription=new String[10];
            String[]icon=new String[10];
            String[]time=new String[10];
            weathers = new ArrayList<>();
            for (int i=0; i<response.getList().size();i++){
                humidity[i] = String.valueOf(response.getList().get(i).getMain().getHumidity());
                raindescription[i] = String.valueOf(response.getList().get(i).getWeather().get(0).getDescription());
                icon[i] = String.valueOf(response.getList().get(i).getWeather().get(0).getIcon());
                time[i] = String.valueOf(response.getList().get(i).getDt());

                Log.w("humidity",humidity[i]);
                Log.w("rain_description",raindescription[i]);
                Log.w("icon",icon[i]);
                Log.w("time",time[i]);

                weathers.add(new Weather(String.valueOf(response.getList().get(i).getWeather().get(0).getIcon()), String.valueOf(response.getList().get(i).getMain().getHumidity()), String.valueOf(response.getList().get(i).getWeather().get(0).getDescription()), String.valueOf(response.getList().get(i).getDt())));

            }
            mAdapter = new WeatherAdapter(weathers,weatherFont);
            mRecyclerView.setAdapter(mAdapter);

            spinner.setVisibility(View.GONE);


        }

        @Override
        public void failure (RetrofitError error) {

            place.setText("Unable to retrieve data");

            spinner.setVisibility(View.GONE);


            Log.d("", "failure", error);

        }
    };



}