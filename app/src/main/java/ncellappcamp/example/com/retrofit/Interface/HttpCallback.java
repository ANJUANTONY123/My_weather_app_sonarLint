package ncellappcamp.example.com.retrofit.Interface;


import ncellappcamp.example.com.retrofit.Model.Weather;


public interface HttpCallback {

    public void onSuccess (Weather response);

    public void onFailure (String error);

}