package ncellappcamp.example.com.retrofit;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Date;
import java.util.List;



public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.WeatherViewHolder> {

    Typeface weatherfont;
    Context ctx;
    public static class WeatherViewHolder extends RecyclerView.ViewHolder {



        CardView cv;
        TextView weathericon;
        TextView humidity;
        TextView raindescription;
        TextView time;
        Typeface weatherfont;


        WeatherViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.recycler_id);
            weathericon = (TextView)itemView.findViewById(R.id.weather_icon_text);
            humidity = (TextView)itemView.findViewById(R.id.humidity_TextView);
            raindescription = (TextView)itemView.findViewById(R.id.rain_description__TextView);
            time = (TextView)itemView.findViewById(R.id.time_TextView);

        }
    }

    List<Weather> weather;

    WeatherAdapter(List<Weather> weathers,Typeface font){
        Log.w("strong", String.valueOf(weathers.get(0).weathericon));
        this.weatherfont=font;
        this.weather=weathers;
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public WeatherViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v;
        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cardview_forecast, viewGroup, false);
        WeatherViewHolder pvh = new WeatherViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(WeatherViewHolder weatherViewHolder, int i) {

        weatherViewHolder.weathericon.setTypeface(weatherfont);

        switch (weather.get(i).weathericon){
            case "01d":
                weatherViewHolder.weathericon.setText(R.string.wi_day_sunny);
                break;
            case "02d":
                weatherViewHolder.weathericon.setText(R.string.wi_cloudy_gusts);
                break;
            case "03d":
                weatherViewHolder.weathericon.setText(R.string.wi_cloud_down);
                break;
            case "10d":
                weatherViewHolder.weathericon.setText(R.string.wi_day_rain_mix);
                break;
            case "11d":
                weatherViewHolder.weathericon.setText(R.string.wi_day_thunderstorm);
                break;
            case "13d":
                weatherViewHolder.weathericon.setText(R.string.wi_day_snow);
                break;
            case "01n":
                weatherViewHolder.weathericon.setText(R.string.wi_night_clear);
                break;
            case "04d":
                weatherViewHolder.weathericon.setText(R.string.wi_cloudy);
                break;
            case "04n":
                weatherViewHolder.weathericon.setText(R.string.wi_night_cloudy);
                break;
            case "02n":
                weatherViewHolder.weathericon.setText(R.string.wi_night_cloudy);
                break;
            case "03n":
                weatherViewHolder.weathericon.setText(R.string.wi_night_cloudy_gusts);
                break;
            case "10n":
                weatherViewHolder.weathericon.setText(R.string.wi_night_cloudy_gusts);
                break;
            case "11n":
                weatherViewHolder.weathericon.setText(R.string.wi_night_rain);
                break;
            case "13n":
                weatherViewHolder.weathericon.setText(R.string.wi_night_snow);
                break;
            default:
                weatherViewHolder.weathericon.setText("-=");


        }
        Log.w("time:",weather.get(i).time);
        long timestamp = Long.parseLong(weather.get(i).time);
        Date expiry = new Date(timestamp * 1000);
        weatherViewHolder.time.setText(String.valueOf(expiry));

        weatherViewHolder.raindescription.setText(weather.get(i).raindescr);
        weatherViewHolder.humidity.setText("humidity:"+weather.get(i).humidity);
    }

    @Override
    public int getItemCount() {
        return weather.size();
    }
}